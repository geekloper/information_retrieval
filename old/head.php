<?php

$separateurs = ",.;\'() ";

// lecture d'une source Html 
$chaine_html = implode( file('source.html'),' ');

// recouperation du descriptif
$description = get_description("source.html");

// recuperation des keywords
$keywords = get_keywords("source.html");

// recuperation du title
$modele = "/<title>(.*)<\/title>/i";
$title = get_title($modele,$chaine_html);

// construction de la chaine head
$chaine_head = strtolower($title." ".$description." ".$keywords);

// segmentation du texte en mots
$tab_mots = explode_bis($separateurs, $chaine_head);

print_array($tab_mots);

//filtrage de doublons et obtention de nombre d'occurrences
$tab_mots_soccurrences = array_count_values($tab_mots);
print_array($tab_mots_soccurrences);


function get_description($source){
	
		$table_metas = get_meta_tags($source);

		if(  $table_metas['description'] )
			 return $table_metas['description'];
		else return "";
	
}

function get_keywords($source){
	
		$table_metas = get_meta_tags($source);

		if(  $table_metas['keywords'] )
			 return $table_metas['keywords'];
		else return "";
	
}


function get_title($modele,$chaine){
	
	
preg_match($modele,$chaine,$tableau_res);

if($tableau_res[1])
	return $tableau_res[1];
else return "";
	
	
}

function explode_bis($separateurs, $chaine){
                $tab = array();
                $tok= strtok($chaine, $separateurs);
                    if (strlen($tok) > 2) $tab[] = $tok;

                    while ( $tok !== false )
                        {
                            
                            $tok = strtok($separateurs);
                            if (strlen($tok) > 2) $tab[] = $tok;
                        }
                    return $tab;
}
        
function print_array($Token){
            
            foreach ( $Token as $cle => $valeur)
                
                echo "[ $cle ]=",$valeur,"<br>";
}


?>

