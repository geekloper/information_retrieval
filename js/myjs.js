function modal_wordcloud(id_document) {

			$('#wordcloud').empty();

			$.getJSON("ajax_req.php?id_document=" + id_document,

			    function(data){

			      d3.wordcloud()
			        .size([600, 300])
			        .selector('#wordcloud')
			        .words(data)
			        .start();
			            
			    });

			$('#exampleModalCenter').modal('show');
}