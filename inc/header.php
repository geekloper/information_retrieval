<!doctype html>
<html lang="en">
  <head>
    <title>Information retrieval</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <style type="text/css">
      .form-home{
          margin-top: 30%;
          margin-left: auto;
          margin-right: auto;
          max-width: 500px;
      }
    </style>
  </head>
  <body>
    <?php if(basename($_SERVER['PHP_SELF']) == "result.php"){ ?>
    <nav aria-label="breadcrumb" role="navigation">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="home.php">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Result</li>
      </ol>
    </nav>
    <?php } ?>  