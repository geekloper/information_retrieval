<?php

function get_body($chaine_html){

  $modele = "/<body[^>]*>(.*)<\/body>/is";

  preg_match($modele,$chaine_html,$tableau_res);

  if($tableau_res[1]){

    //suppression du javascript du body
    $modele = "/<script[^>]*?>.*?<\/script>/is";
    $chaine_html_sans_script = preg_replace($modele,"",$tableau_res[1]);

    //suppression du style du body
    $modele = "/<style[^>]*?>.*?<\/style>/is";
    $chaine_html_sans_script = preg_replace($modele,"",$chaine_html_sans_script);

    //suppression de balises html du body -> body en texte brute
    $chaine_body_texte = strip_tags($chaine_html_sans_script);

    //mise en minuscule du texte avant traitement
    $chaine_body_texte = strtolower($chaine_body_texte);

    // segmentation du texte en mots
    $tab_mots_body = explode_bis($chaine_body_texte);

    //filtrage de doublons et obtention de nombre d'occurrences
    return array_count_values($tab_mots_body);

  }else{

    return "";

  }
                    
}


function explode_bis($chaine , $separateurs = ",.;\'()/\n/\S 0123456789€\?\!:{}_"){

  $tab_banned_list = get_banned_keywords();
  $tab_final = array();
  $tok= strtok($chaine, $separateurs);
 
  if (strlen($tok) > 2 && !is_banned_keyword($tok,$tab_banned_list)) 
    $tab_final[] = $tok;

  while ( $tok !== false )
      {
          
          $tok = strtok($separateurs);
          if (strlen($tok) > 2 && !is_banned_keyword($tok,$tab_banned_list)) 
            $tab_final[] = $tok;
      }
  return $tab_final;
}
        
function print_array($Token){

  foreach ( $Token as $cle => $valeur)
    echo "[ $cle ]=",$valeur,"<br>";

}

function is_banned_keyword($keyword,$tab_banned_list){
  return in_array($keyword, $tab_banned_list);
}


function get_banned_keywords(){

  $ban_list_file = "banned_keywords.txt";

  return file($ban_list_file, FILE_IGNORE_NEW_LINES);

}

function get_description($source){
  
    $table_metas = get_meta_tags($source);
    
    return ($table_metas['description'] ? $table_metas['description'] : "" );
}

function get_keywords($source){
  
    $table_metas = get_meta_tags($source);

    return ($table_metas['keywords'] ? $table_metas['keywords'] : "" );
  
}


function get_title($chaine){
  
  $modele = "/<title>(.*)<\/title>/i";

  preg_match($modele,$chaine,$tableau_res);

  return ($tableau_res[1] ? $tableau_res[1] : "" );

}

function get_head($chaine, $source){
  // recouperation du descriptif
  $description = get_description($source);

  // recuperation des keywords
  $keywords = get_keywords($source);

  // recuperation du title
  $title = get_title($chaine);

  // construction de la chaine head
  $chaine_head = strtolower($title." ".$description." ".$keywords);

  // segmentation du texte en mots
  $tab_mots = explode_bis($chaine_head);

  //filtrage de doublons et obtention de nombre d'occurrences
  return array_count_values($tab_mots);
}


function get_all_doc($chaine , $source){

  //recupération mots body + occurences
  $tab_body_mots_occurrences = get_body($chaine);

  // recupération mots head + occurences
  $tab_head_mots_occurrences = get_head($chaine , $source);

  $merged = array();
  
  // duplicate $tab_head_mots_occurrences to get weight
  foreach ([$tab_body_mots_occurrences, $tab_head_mots_occurrences , $tab_head_mots_occurrences] as $a) {    // iterate both arrays
      foreach ($a as $key => $value) {                                          // iterate all keys+values
          $merged[$key] = $value + ($merged[$key] ?? 0);                        // merge and add
      }
  }

  return $merged;

}

?>
