<?php 

$tab1 = array( "foo" => 1 , "bar" => 2 , "foobar" => 5 , "barbar" => 3);
$tab2 = array( "foo" => 4 , "bar" => 1 , "barfoo" => 6 , "barbar" => 3);

// Exepted results : 
// foo 5 ,  bar 3 , foobar 5 , barfoo 6

print_r(fusion_tab1_tab2($tab1 , $tab2));

function fusion_tab1_tab2($tab1 , $tab2){

  $merged = array();
  
  foreach ([$tab1, $tab2] as $a) {
      foreach ($a as $key => $value) {
          $merged[$key] = $value + ($merged[$key] ?? 0);                        // merge and add
      }
  }

  return $merged;
}




?>