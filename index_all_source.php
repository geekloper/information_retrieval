<?php

require 'inc/functions.inc.php';
require 'inc/connection.php';

//Test Main function
//main('source.html');

function main($source){
    require 'inc/connection.php';
    // lecture d'une source Html
    $chaine_html = implode( file($source),' ');

    $description = get_description($source);
    $title = get_title($chaine_html);

    // récupération mots de tout le document + occurences
    $tab_all_mots_poids = get_all_doc($chaine_html , $source);


    $sql_document = "INSERT INTO document (document,titre,description) VALUES('$source','$title','$description')";
    $conn->query($sql_document);
    $id_document = $conn->lastInsertId();

    //Insérer dans la base de données
    foreach ($tab_all_mots_poids as $mot_=>$poids) {

      $mot = mb_convert_encoding($mot_, "UTF-8");

      $mot_exist = $conn->query( "SELECT id FROM mot WHERE mot = '$mot' " )->fetch();

      if(!$mot_exist){

        $sql_mot = "INSERT INTO mot (mot) VALUES ('$mot')";
        $conn->query($sql_mot);
        $id_mot = $conn->lastInsertId();

      }
      else{

        $id_mot = $mot_exist[0];

      }
      
      $sql_mot_document = "INSERT INTO mot_document (id_mot,id_document,poids) VALUES ($id_mot,$id_document,$poids)";
      $conn->query($sql_mot_document);
    }

}



?>